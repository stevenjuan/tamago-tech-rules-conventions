

# Communications in Slack

For tech team, try to use Slack to replace WhatsApp.

## Channels

[naming of slacks](https://get.slack.help/hc/en-us/articles/217626408-Create-guidelines-for-channel-names)

* `#scrum_report`: to do the daily
* `#engineering`: for all engineers

## Self Management

1. Make sure you have in progress task in the boards
   1. [Kaban Board](https://tamagoegg.atlassian.net/secure/RapidBoard.jspa?projectKey=TL&rapidView=7)
   2. [Sprint Board](https://tamagoegg.atlassian.net/secure/RapidBoard.jspa?rapidView=8&projectKey=TL)

### Prefix
1. `event-`: for events
2. `st-`: for features
3. `bug-`: for bugs, only the large one, which needs discussion
4. `ver-`: In the future, when we have the explicit version of the product, we create a `ver-"number"` for version integration