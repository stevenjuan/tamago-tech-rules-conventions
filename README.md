# Rules

The rules are built to follow.

## Daily Meetings

1. Every Day, Same Place, Same Time
2. 3 Things:
    1. What I did yesterday
    2. What I plan to do today
    3. What impediments do I have'
3. Every day before standup, make sure your jira ticket is updated (move to a different status, comment progress/impedance). As this will make our daily standup faster and more efficient. 

### For those who work remotely

In normal cases, 

1. You MUST update the progress by text in the #scrum_report channel BEFORE the team starts meeting
2. You MUST stay online, and the team might call you if the discussion needs you to involve

For those people cannot do above 2 rules, please STATE YOUR REASON when you can update.

Note: The quality of the conferencing call is usually bad if many people join at the same time, so it's not a default option. Let the coach know if you want to participate thru conferencing call.

#### Template of Daily

* What I did yesterday?
  1. (#Ticket Number (Optional))"The task" 
* What I plan to today?
  1. (#Ticket Number (Optional))"The task" 
* What impediments do I have?
  1. Discussion
* (Optional) Why I can't stay online in the meeting
  1. Some reasons, e.g., meeting with partners, internet problems ...etc

## Code Commit

1. DO NOT commit to the Master Branch directly. Create PULL REQUEST to merge.
2. DO TEST YOUR CODE BEFORE YOU MERGE. Manually test, compile, do all the necessary tests before you create PR.
3. DO NOT create PR from your private repo.